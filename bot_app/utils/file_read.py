from os import path
from main_app.settings import ROOT_DIR


def get_file_entry(name, root_dir=ROOT_DIR, data_dir=None, joined=False):
    work_dir = root_dir if data_dir is None else path.join(root_dir, data_dir)
    with open(path.join(work_dir, name), 'r', encoding='utf-8') as fh:
        res = fh.readlines()
    if joined:
        return ''.join(res)
    return res
