from os.path import join
from aiogram import Dispatcher
from main_app.app_log import get_logger
from main_app.settings import ROOT_DIR, LOG_FILE
from main_app.config import BOT_ADMIN, BOT_NAME


async def startup_notify(dp: Dispatcher):
    try:
        msg = f'{BOT_NAME}  started'
        await dp.bot.send_message(BOT_ADMIN, msg)
    except Exception as e:
        logger = get_logger(join(ROOT_DIR, LOG_FILE), __name__)
        logger.error(str(e), exc_info=False)


async def standard_notify(dp: Dispatcher, msg):
    try:
        await dp.bot.send_message(BOT_ADMIN, msg)
    except Exception as e:
        logger = get_logger(join(ROOT_DIR, LOG_FILE), __name__)
        logger.error(str(e), exc_info=False)
