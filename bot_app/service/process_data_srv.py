import random
from main_app.settings import MEOW_DATA, DATA_DIR
from utils.file_read import get_file_entry


def get_result():
    answers = get_file_entry(MEOW_DATA, data_dir=DATA_DIR, joined=False)
    pos = random.randint(0, len(answers) - 1)
    return answers[pos].strip()
