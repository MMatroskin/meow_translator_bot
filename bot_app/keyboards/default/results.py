from aiogram.types import ReplyKeyboardMarkup
from .menu import line


results_keyboard = ReplyKeyboardMarkup(
    keyboard=[
        line,
    ],
    resize_keyboard=True
)
