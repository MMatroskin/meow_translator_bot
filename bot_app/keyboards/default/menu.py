from aiogram.types import ReplyKeyboardMarkup, KeyboardButton


line = [
    KeyboardButton(text='Help'),
    KeyboardButton(text='Meow'),
]

keyboard = [
    line,
]

menu = ReplyKeyboardMarkup(
    keyboard=keyboard,
    resize_keyboard=True
)
