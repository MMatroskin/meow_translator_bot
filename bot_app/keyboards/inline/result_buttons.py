from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from keyboards.inline.callback_datas import get_callback_button
from main_app.icons import icons
from main_app.settings import MAX_DATA_LEN


def get_keyboard(data):
    icon_active = icons[6].image
    msg = data if len(data) < MAX_DATA_LEN else 'meow!'
    cb_data = get_callback_button.new(
        data=msg
    )
    keys = [
        [
            InlineKeyboardButton(
                text=f'{icon_active} Да!',
                callback_data=cb_data),
        ]
    ]
    keyboard = InlineKeyboardMarkup(inline_keyboard=keys)
    return keyboard
