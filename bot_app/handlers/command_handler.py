from io import BytesIO
import gc
import logging
import random
from os.path import join
from aiogram import types
from aiogram.dispatcher.filters import Command, CommandStart, CommandHelp
from bot.bot import dp, bot
from keyboards.default.menu import menu
from keyboards.inline.result_buttons import get_keyboard
from service.process_data_srv import get_result
from utils.file_read import get_file_entry
from utils.notify import standard_notify
from main_app.app_log import get_logger
from main_app.settings import HELP_DATA, ANS_DATA, ROOT_DIR, DATA_DIR, LOG_FILE, ERROR_MSG
from main_app.config import BOT_ADMIN
from main_app.common import msg_base_list
from main_app.icons import icons


@dp.message_handler(CommandStart())
async def process_start_command(message: types.Message, is_restart=False):
    msg = f'{msg_base_list[0].strip()} {icons[1].image}'
    if is_restart:
         msg = f'{msg_base_list[1].strip()} {icons[3].image}'
    await message.answer(msg, reply_markup=menu)


@dp.message_handler(CommandHelp())
async def process_help_command(message: types.Message):
    msg = get_file_entry(HELP_DATA, data_dir=DATA_DIR, joined=True)
    await message.answer(msg, reply_markup=menu)


@dp.message_handler(Command(['report']))
async def process_report_command(message: types.Message):
    if message.from_user.id != int(BOT_ADMIN):
        msg = f'User @{message.from_user.username} is wishes strange!'
        await standard_notify(dp, msg)
        await message.answer(f'{msg_base_list[2].strip()}', reply_markup=menu)
    else:
        name = LOG_FILE
        msg = f"{message.text[1:]}: errors for the current time"
        empty_msg = f'{msg}\nNothing :('
        try:
            ent = get_file_entry(name, joined=True)
            if len(ent) > 0:
                file = BytesIO(bytes(ent, encoding='utf-8'))
                file.name = name
                await bot.send_document(BOT_ADMIN, file, caption=msg, reply_markup=menu)
                file.close()
                del file
            else:
                await send_response_on_error(message, empty_msg)
        except Exception as e:
            logger = get_logger(join(ROOT_DIR, LOG_FILE), __name__)
            logger.error(str(e), exc_info=False)
            await send_response_on_error(message, ERROR_MSG)
        finally:
            gc.collect()


@dp.message_handler(Command(['meow']))
async def process_meow_command(message: types.Message):
    msg_base = get_file_entry(ANS_DATA, data_dir=DATA_DIR)
    data = get_result()
    pos = random.randint(0, len(msg_base) - 1)
    msg = f'{msg_base[pos].strip()}:\n{data}'
    buttons = get_keyboard(data)
    try:
        await bot.send_message(
            message.from_user.id,
            msg,
            reply_markup=buttons)
    except TimeoutError as e:
        logging.info(str(e))
        await process_start_command(message, True)
    except Exception as e:
        logger = get_logger(join(ROOT_DIR, LOG_FILE), __name__)
        logger.error(str(e), exc_info=False)


async def send_response_on_error(message, res):
    await message.answer(res, reply_markup=menu)
