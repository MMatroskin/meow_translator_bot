from os.path import join
from aiogram import types
from aiogram.utils import exceptions as ag_ex
import logging
from bot.bot import dp, bot
from .command_handler import process_start_command
from main_app.app_log import get_logger
from main_app.settings import ROOT_DIR, LOG_FILE
from main_app.common import msg_base_list
from main_app.icons import icons


@dp.callback_query_handler(lambda c: c.data.startswith('yes'))
async def process_callback_button_next(callback_query: types.CallbackQuery):
    try:
        await bot.answer_callback_query(callback_query.id)
        msg = f'{msg_base_list[5].strip()} {icons[0].image}\n{msg_base_list[6].strip()}'
        await bot.send_message(callback_query.from_user.id, f'{msg}')
    except ag_ex.InvalidQueryID as e:
        logging.info(str(e))
        await process_start_command(callback_query.message, True)
    except Exception as e:
        logger = get_logger(join(ROOT_DIR, LOG_FILE), __name__)
        logger.error(str(e), exc_info=False)
