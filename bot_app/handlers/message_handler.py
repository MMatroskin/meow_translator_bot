from aiogram import types
from aiogram.dispatcher.filters import Text
from asyncio import TimeoutError
from os.path import join
import logging
from bot.bot import dp, bot
from .command_handler import process_start_command, process_help_command, process_meow_command
from main_app.app_log import get_logger
from main_app.settings import ROOT_DIR, LOG_FILE
from main_app.common import msg_base_list
from main_app.icons import icons


@dp.message_handler(Text(equals=['Help', 'Meow']))
async def menu_message_handler(message: types.Message):
    if message.text == 'Help':
        await process_help_command(message)
    else:
        await process_meow_command(message)


@dp.message_handler()
async def other_message_handler(message: types.Message):

    msg = f'{msg_base_list[3].strip()} {icons[2].image}\n{msg_base_list[4].strip()}'
    try:
        await bot.send_message(
            message.from_user.id,
            msg)
    except TimeoutError as e:
        logging.info(str(e))
        await process_start_command(message, True)
    except Exception as e:
        logger = get_logger(join(ROOT_DIR, LOG_FILE), __name__)
        logger.error(str(e), exc_info=False)
