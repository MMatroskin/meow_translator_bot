from aiogram import executor
from os.path import join
import logging
from main_app.settings import LOG_FILE, ROOT_DIR
from main_app.config import USE_WEBHOOK, WEBHOOK_URL, WEBHOOK_PATH, HOST, PORT, ON_WEB
from bot.bot import dp, bot, storage
from utils.notify import startup_notify
from handlers import message_handler, command_handler, callback_handler


async def on_startup(dp):
    logging.info('Bot started')

    if USE_WEBHOOK:
        await bot.set_webhook(WEBHOOK_URL)

    # отправить можно только, если диалог с ботом уже был когда - либо начат
    await startup_notify(dp)


async def on_shutdown(dp):

    if USE_WEBHOOK:
        await bot.delete_webhook()
        await dp.storage.close()
        await dp.storage.wait_closed()
    else:
        await bot.close()
        await storage.close()

    logging.info('Bot stopped')


def main():
    f_handler = logging.FileHandler(join(ROOT_DIR, LOG_FILE), encoding='utf-8')
    f_handler.setLevel(logging.ERROR)
    f_s = '%(asctime)s %(filename)s [LINE:%(lineno)d] #%(levelname)s - %(message)s'
    logging.basicConfig(
        format=f_s,
        level=logging.INFO,
        handlers=[
            f_handler,
            logging.StreamHandler()
        ]
    )

    print(f'\n======== Running ========\n(Press CTRL+Break to quit)\n')

    if USE_WEBHOOK:
        executor.start_webhook(
            dispatcher=dp,
            webhook_path=WEBHOOK_PATH,
            skip_updates=True,
            on_startup=on_startup,
            on_shutdown=on_shutdown,
            host=HOST,
            port=PORT,
        )
    else:
        executor.start_polling(dp, on_startup=on_startup, on_shutdown=on_shutdown, skip_updates=True)


if __name__ == '__main__':
    main()
