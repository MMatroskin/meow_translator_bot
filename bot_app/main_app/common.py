import configparser
from collections import namedtuple
import os
from utils.file_read import get_file_entry
from main_app.settings import ROOT_DIR, DATA_DIR, CONFIG, ANS_BASE


def get_config(filename=CONFIG):
    config_path = os.path.join(ROOT_DIR, filename)
    config = configparser.ConfigParser()
    config.read(config_path)
    return config


Icon = namedtuple('Icon', 'image, value')
loggers = {}
msg_base_list = get_file_entry(ANS_BASE, data_dir=DATA_DIR)
