from pathlib import Path


ROOT_DIR = Path(__file__).resolve().parent.parent
DATA_DIR = r'app_data'
CONFIG = r'app.ini'
PRIVATE_CONFIG = r'.private/private.ini'
HELP_DATA = r'help_data'
MEOW_DATA = r'meow_data'
ANS_BASE = r'answers_base'
ANS_DATA = r'answers'
LOG_FILE = r'log.txt'
MAX_DATA_LEN = 15
ERROR_MSG = r'Internal Error!'
WEBHOOK_SSL_CERT = './private/webhook_cert.pem'  # Path to the ssl certificate
WEBHOOK_SSL_PRIV = './private/webhook_pkey.pem'  # Path to the ssl private key
