import os
from main_app.settings import CONFIG, PRIVATE_CONFIG
from main_app.common import get_config


def get_config_item_value(config, section, item):
    try:
        val = config.get(section, item)
        return val
    except Exception as e:
        val = None
    return val


config = get_config(PRIVATE_CONFIG)

BOT_TOKEN = os.getenv('BOT_TOKEN')
if not BOT_TOKEN:
    BOT_TOKEN = get_config_item_value(config, 'bot', 'token')
    if not BOT_TOKEN:
        print('You have forgot to set BOT_TOKEN')
        quit()

BOT_ADMIN = os.getenv('BOT_ADMIN')
if not BOT_ADMIN:
    BOT_ADMIN = get_config_item_value(config, 'bot', 'admin')
    if not BOT_ADMIN:
        print('You have forgot to set BOT_ADMIN')
        quit()

BOT_NAME = os.getenv('BOT_NAME')
if not BOT_NAME:
    BOT_NAME = get_config_item_value(config, 'bot', 'name')
    if not BOT_ADMIN:
        print('You have forgot to set BOT_NAME')
        quit()

# sentry logging
SENTRY_URL = os.getenv('SENTRY_URL')
if not SENTRY_URL:
    SENTRY_URL = get_config_item_value(config, 'logging', 'sentry_url')

# webserver settings
config = get_config(CONFIG)
HOST = config.get("app", "host")
PORT = int(config.get("app", "port"))
ON_WEB = bool(int(os.getenv('ON_WEB', 0)))
if ON_WEB:
    PORT = int(os.getenv("PORT"))

# sentry logging settings
USE_SENTRY = bool(int(config.get("app", "use_sentry")))

# webhook settings
USE_WEBHOOK = bool(int(config.get("app", "use_webhook")))
PUBLIC_APP_LINK = os.getenv('PUBLIC_APP_NAME')
if ON_WEB and not PUBLIC_APP_LINK:
    print('You have forgot to set PUBLIC_APP_LINK')
    quit()
WEBHOOK_HOST = f'https://{PUBLIC_APP_LINK}'
WEBHOOK_PATH = f'/webhook/{BOT_TOKEN}'
WEBHOOK_URL = f'{WEBHOOK_HOST}{WEBHOOK_PATH}'
