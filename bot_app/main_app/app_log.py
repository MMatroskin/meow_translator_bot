import logging
import sentry_sdk
from sentry_sdk.integrations.logging import EventHandler
from main_app.common import loggers
from main_app.config import USE_SENTRY, SENTRY_URL


def get_logger(file_name, name):
    log = loggers.get(name, None)
    if log is None:
        level = logging.ERROR
        f_s = '%(asctime)s %(filename)s [LINE:%(lineno)d] #%(levelname)s - %(message)s'

        f = logging.Formatter(f_s, datefmt='%d-%m-%Y %H:%M:%S')

        f_handler = logging.FileHandler(file_name, encoding='utf-8')
        f_handler.setFormatter(f)
        f_handler.setLevel(level)

        log = logging.getLogger(name)
        log.setLevel(logging.INFO)
        log.addHandler(f_handler)

        if USE_SENTRY:
            sentry_sdk.init(
                dsn=SENTRY_URL,
                attach_stacktrace=True,
                default_integrations=False
            )
            s_handler = EventHandler(logging.ERROR)
            s_handler.setFormatter(f)
            log.addHandler(s_handler)

        log.propagate = False

        loggers[name] = log

    return log
